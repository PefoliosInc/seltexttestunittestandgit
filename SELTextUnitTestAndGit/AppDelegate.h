//
//  AppDelegate.h
//  SELTextUnitTestAndGit
//
//  Created by Mykola Mamonchuk on 8/8/17.
//  Copyright © 2017 Mykola Mamonchuk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

